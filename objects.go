// Copyright (C) 2011, Ross Light

package pdf

import (
	"strconv"
)

// name is a PDF name object, which is used as an identifier.
type name string

func (n name) String() string {
	return string(n)
}

func (n name) marshalPDF(dst []byte) ([]byte, error) {
	// TODO: escape characters
	dst = append(dst, '/')
	return append(dst, n...), nil
}

type indirectObject struct {
	Reference
	Object interface{}
}

const (
	objectBegin = " obj\r\n"
	objectEnd   = "\r\nendobj"
)

func (obj indirectObject) marshalPDF(dst []byte) ([]byte, error) {
	var err error
	dst = strconv.AppendUint(dst, uint64(obj.Number), 10)
	dst = append(dst, ' ')
	dst = strconv.AppendUint(dst, uint64(obj.Generation), 10)
	dst = append(dst, objectBegin...)
	if dst, err = marshal(dst, obj.Object); err != nil {
		return dst, err
	}
	dst = append(dst, objectEnd...)
	return dst, nil
}

// Reference holds a PDF indirect reference.
type Reference struct {
	Number     uint
	Generation uint
}

func (ref Reference) marshalPDF(dst []byte) ([]byte, error) {
	dst = strconv.AppendUint(dst, uint64(ref.Number), 10)
	dst = append(dst, ' ')
	dst = strconv.AppendUint(dst, uint64(ref.Generation), 10)
	dst = append(dst, ' ', 'R')
	return dst, nil
}
