module gitlab.com/TheLinker/gopdf

go 1.15

require (
	golang.org/x/image v0.0.0-20180926015637-991ec62608f3
	golang.org/x/text v0.3.0
)
