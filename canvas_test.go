// Copyright (C) 2011, Ross Light

package pdf

import (
	"testing"
)

const pathExpectedOutput = "12 34 m\n-56 78 l\nh\n3.1 -5.9 21.1 80.9 re\n"

func TestPath(t *testing.T) {
	path := new(Path)
	path.Move(Point{12, 34})
	path.Line(Point{-56, 78})
	path.Close()
	path.Rectangle(Rectangle{Point{3.1, -5.9}, Point{24.2, 75.0}})

	if path.buf.String() != pathExpectedOutput {
		t.Errorf("Output was %q, expected %q", path.buf.String(), pathExpectedOutput)
	}
}
