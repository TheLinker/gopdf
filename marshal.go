// Copyright (C) 2011, Ross Light

package pdf

import (
	"errors"
	"strconv"
	"strings"
)

// A marshaler can produce a PDF object.
type marshaler interface {
	marshalPDF(dst []byte) ([]byte, error)
}

// marshal returns the PDF encoding of v.
//
// If the value implements the marshaler interface, then its marshalPDF method
// is called.  ints, strings, and floats will be marshalled according to the PDF
// standard.
func marshal(dst []byte, v interface{}) ([]byte, error) {
	state := marshalState{dst}

	switch m := v.(type) {
	case marshaler:
		return m.marshalPDF(dst)
	case int:
		state.data = strconv.AppendInt(state.data, int64(m), 10)
		return state.data, nil
	case Unit:
		state.data = strconv.AppendFloat(state.data, float64(m), 'f',
			marshalFloatPrec, 32)
		return state.data, nil
	case float32:
		state.data = strconv.AppendFloat(state.data, float64(m), 'f',
			marshalFloatPrec, 32)
		return state.data, nil
	case []Unit:
		return state.marshalUnitSlice(m)
	case []name:
		return state.marshalNameSlice(m)
	case []Reference:
		return state.marshalReferenceSlice(m)
	case string:
		return state.writeQuote(m)
	}
	return state.data, errors.New("pdf: unsupported type")
}

func (state *marshalState) marshalUnitSlice(xs []Unit) ([]byte, error) {
	state.writeString("[ ")
	for _, x := range xs {
		state.data = strconv.AppendFloat(state.data, float64(x), 'f',
			marshalFloatPrec, 32)
		state.writeString(" ")
	}
	state.writeString("]")
	return state.data, nil
}

func (state *marshalState) marshalNameSlice(ss []name) ([]byte, error) {
	state.writeString("[ ")
	for _, s := range ss {
		state.data, _ = s.marshalPDF(state.data)
		state.writeString(" ")
	}
	state.writeString("]")
	return state.data, nil
}

func (state *marshalState) marshalReferenceSlice(
	rs []Reference,
) ([]byte, error) {
	state.writeString("[ ")
	for _, r := range rs {
		state.data, _ = r.marshalPDF(state.data)
		state.writeString(" ")
	}
	state.writeString("]")
	return state.data, nil
}

type marshalState struct {
	data []byte
}

const marshalFloatPrec = -1

func (state *marshalState) writeString(s string) {
	state.data = append(state.data, s...)
}

var quoteReplacer = strings.NewReplacer(
	"\r", `\r`,
	"\t", `\t`,
	"\b", `\b`,
	"\f", `\f`,
	"(", `\(`,
	")", `\)`,
	`\`, `\\`,
)

// writeQuote escapes a string and writes a PDF string literal.
func (state *marshalState) writeQuote(s string) ([]byte, error) {
	state.data = append(state.data, '(')
	state.writeString(quoteReplacer.Replace(s))
	return append(state.data, ')'), nil
}

func (state *marshalState) marshalKeyValue(k name, v interface{}) error {
	state.data, _ = k.marshalPDF(state.data)
	state.writeString(" ")

	var err error
	state.data, err = marshal(state.data, v)
	if err != nil {
		return err
	}

	state.writeString(" ")
	return nil
}
