package pdf

import (
	"bytes"
	"compress/zlib"
)

var zPool = make(chan *zlib.Writer, 20)

func zPoolGet(buf *bytes.Buffer) *zlib.Writer {
	var w *zlib.Writer
	select {
	case w = <-zPool:
		w.Reset(buf)
	default:
		w, _ = zlib.NewWriterLevel(buf, 8)
	}
	return w
}

func zPoolPut(w *zlib.Writer) {
	select {
	case zPool <- w:
	default:
	}
}
