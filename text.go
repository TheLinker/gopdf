// Copyright (C) 2011, Ross Light

package pdf

import (
	"bytes"
	"strings"

	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

var windows1252 = charmap.Windows1252.NewEncoder()

// Text is a PDF text object.  The zero value is an empty text object.
type Text struct {
	buf   bytes.Buffer
	fonts map[name]bool

	x, y        Unit
	currFont    name
	currSize    Unit
	currLeading Unit
}

// Reset resets the position of the text object.
func (text *Text) Reset() {
	text.x = 0
	text.y = 0
}

// Text adds a string to the text object.
func (text *Text) Text(s string) {
	// TODO: check error
	str, _, _ := transform.String(windows1252, s)

	writeCommand(&text.buf, "Tj", str)
	if widths := getFontWidths(text.currFont); widths != nil {
		text.x += computeStringWidth(s, widths, text.currSize)
	}
}

// WordSpacing sets the sizes of spaces between words.
func (text *Text) WordSpacing(spc Unit) {
	writeCommand(&text.buf, "Tw", spc)
}

// Width computes the widht of the string with the current font.
func (text *Text) Width(s string) Unit {
	var width Unit
	if widths := getFontWidths(text.currFont); widths != nil {
		width += computeStringWidth(s, widths, text.currSize)
	}
	return width
}

const defaultLeadingScalar = 1.2

// SetFont changes the current font to a standard font.  This also changes the
// leading to 1.2 times the font size.
func (text *Text) SetFont(fontName string, size Unit) {
	if text.fonts == nil {
		text.fonts = make(map[name]bool)
	}
	text.fonts[name(fontName)] = true
	text.currFont, text.currSize = name(fontName), size
	writeCommand(&text.buf, "Tf", name(fontName), size)
	text.SetLeading(size * defaultLeadingScalar)
}

// SetLeading changes the amount of space between lines.
func (text *Text) SetLeading(leading Unit) {
	writeCommand(&text.buf, "TL", leading)
	text.currLeading = leading
}

// NextLine advances the current text position to the next line, based on the
// current leading.
func (text *Text) NextLine() {
	writeCommand(&text.buf, "T*")
	text.x = 0
	text.y -= text.currLeading
}

// NextLineOffset moves the current text position to an offset relative to the
// beginning of the line.
func (text *Text) NextLineOffset(tx, ty Unit) {
	writeCommand(&text.buf, "Td", tx, ty)
	text.x = tx
	text.y += ty
}

// X returns the current x position of the text cursor.
func (text *Text) X() Unit {
	return text.x
}

// Y returns the current y position of the text cursor.
func (text *Text) Y() Unit {
	return text.y
}

// Standard 14 fonts
const (
	Courier            = "Courier"
	CourierBold        = "Courier-Bold"
	CourierOblique     = "Courier-Oblique"
	CourierBoldOblique = "Courier-BoldOblique"

	Helvetica            = "Helvetica"
	HelveticaBold        = "Helvetica-Bold"
	HelveticaOblique     = "Helvetica-Oblique"
	HelveticaBoldOblique = "Helvetica-BoldOblique"

	Symbol = "Symbol"

	Times           = "Times-Roman"
	TimesBold       = "Times-Bold"
	TimesItalic     = "Times-Italic"
	TimesBoldItalic = "Times-BoldItalic"

	ZapfDingbats = "ZapfDingbats"
)

var fontToWidths = map[name][]uint16{
	Courier:              courierWidths,
	CourierBold:          courierBoldWidths,
	CourierOblique:       courierObliqueWidths,
	CourierBoldOblique:   courierBoldObliqueWidths,
	Helvetica:            helveticaWidths,
	HelveticaOblique:     helveticaObliqueWidths,
	HelveticaBold:        helveticaBoldWidths,
	HelveticaBoldOblique: helveticaBoldObliqueWidths,
	Symbol:               symbolWidths,
	Times:                timesRomanWidths,
	TimesBold:            timesBoldWidths,
	TimesItalic:          timesItalicWidths,
	TimesBoldItalic:      timesBoldItalicWidths,
	ZapfDingbats:         zapfDingbatsWidths,
}

func getFontWidths(fontName name) []uint16 {
	return fontToWidths[fontName]
}

var replacer = strings.NewReplacer(
	"Á", "A",
	"É", "E",
	"Í", "I",
	"Ó", "O",
	"Ú", "U",
	"Ñ", "N",
	"á", "a",
	"é", "e",
	"í", "i",
	"ó", "o",
	"ú", "u",
	"ñ", "n",
)

func computeStringWidth(s string, widths []uint16, fontSize Unit) Unit {
	s = replacer.Replace(s)
	width := Unit(0)
	for _, r := range s {
		if int(r) < len(widths) {
			width += Unit(widths[r])
		}
	}
	return width * fontSize / 1000
}
